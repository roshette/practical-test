const Provider = require("../models/provider.js");

exports.createProvider = async (request, response) => {
  if (!request.body) return response.sendStatus(400);

  const providerName = request.body.name;
  const provider = new Provider({ name: providerName });
  try {
    let newProvider = await provider.save();
    response.send(newProvider);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.getProviders = async (request, response) => {
  try {
    const providers = await Provider.find({});
    response.send(providers);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.updateProvider = async (request, response) => {
  if (!request.body) return response.sendStatus(400);

  const providerId = request.body.id;
  const providerName = request.body.name;

  try {
    const provider = await Provider.findOneAndUpdate(
      { _id: providerId },
      { name: providerName },
      { new: true }
    );
    response.send(provider);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.deleteProvider = async (request, response) => {
    const providerId = request.body.id;

    try {   
        await Provider.deleteOne({ _id: providerId })
        response.sendStatus(204);
    } catch(e) {
        console.error(e);
        response.sendStatus(500);
    }
}
