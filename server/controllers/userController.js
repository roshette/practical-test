const User = require("../models/user.js");

exports.getUsers = async (request, response) => {
  try {
    let users = await User.find({});
    response.send(users);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.createUser = async (request, response) => {
  if (!request.body) return response.sendStatus(400);

  const userName = request.body.name;
  const userEmail = request.body.email;
  const userPhone = request.body.phone;
  const userProviders = request.body.providers;

  const user = new User({
    name: userName, 
    email: userEmail,
    phone: userPhone,
    providers: userProviders,
  });

  try {
    let newUser = await user.save();
    response.send(newUser);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.updateUser = async (request, response) => {
  if (!request.body) return response.sendStatus(400);

  const userId = request.body.id;
  const userName = request.body.name;
  const userEmail = request.body.email;
  const userPhone = request.body.phone;
  const userProviders = request.body.providers;

  try {
    const user = await User.findOneAndUpdate(
      { _id: userId },
      {
        name: userName,
        email: userEmail,
        phone: userPhone,
        providers: userProviders,
      },
      { new: true }
    );
    response.send(user);
  } catch (e) {
    console.error(e);
    response.sendStatus(500);
  }
};

exports.deleteUser = async (request, response) => {
  const userId = request.body.id;
  try {
    await User.deleteOne({ _id: userId });
    response.sendStatus(204);
  } catch (e) {
    console.error(e);
  }
};
