const express = require("express");
const providerController = require("../controllers/providerController.js");
const providerRouter = express.Router();

/**
 * @swagger
 * /providers/:
 *    get:
 *      description: Use to return all providers
 *      responses:
 *        '200':
 *          description: return array providers
 *        '500':
 *          description: error occured
 */
providerRouter.get("/", providerController.getProviders);

/**
 * @swagger
 * /providers/:
 *    post:
 *      description: Use to create new provider
 *      parameters:
 *        - name: name
 *          description: Name of provider
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *      responses:
 *        '200':
 *          description: return created provider
 *        '500':
 *          description: error occured
 */
providerRouter.post("/", providerController.createProvider);

/**
 * @swagger
 * /providers/:
 *    put:
 *      description: Use to update provider by id
 *      parameters:
 *        - name: id
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *      responses:
 *        '200':
 *          description: return updated provider
 *        '500':
 *          description: error occured
 */
providerRouter.put("/", providerController.updateProvider);

/**
 * @swagger
 * /providers/:
 *    delete:
 *      description: Use to delete provider by id
 *      parameters:
 *        - name: id
 *          required: true
 *          schema:
 *            type: string
 *            format: string
 *      responses:
 *        '204':
 *          description: status successfully deleted provider
 *        '500':
 *          description: error occured
 */
providerRouter.delete("/", providerController.deleteProvider);

module.exports = providerRouter;
