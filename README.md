# Practical test

## Requirement:
- Node.js (v12.16.2 or higher)
- NPM (v6.14.5 or higher)
- MongoDB (v4.2 or higher)

## Dependencies installation

- node.js & npm:
```
sudo curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt-get install -y nodejs
```

- MongoDB:
```
sudo apt update
sudo apt install -y mongodb
```

## Build and Run
1. ``` git clone https://gitlab.com/roshette/practical-test```
2. ```cd practical-test/client ```
3. ``` npm i ```
4. ``` npm run prod ```
5. ``` cd ../server ```
6. ``` npm i ```
7. ``` node app.js ```

## API
- API Swagger documentation: http://localhost:3000/api-docs